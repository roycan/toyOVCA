class Employee < ApplicationRecord


  def self.import(file)



    CSV.foreach(file.path, :headers => true, :col_sep => ';') do |row|
      if row.size < 2
          CSV.foreach(file.path, :headers => true, :col_sep => ',') do |row2|
            import2(row2)
          end
          break
      else
        import2(row)
      end

    end
  end

  def self.import2(row)
    t = Employee.new()
    t.employee_no = row[1]
    t.employee_name = row[2]
    t.date_of_birth = row[3]
    t.gender = row[4]
    t.primary_email_address = row[5]
    t.secondary_email_address = row[6]
    t.primary_contact_no = row[7]
    t.secondary_contact_no = row[8]
    t.permanent_address = row[9]
    t.unit = row[10]
    t.department = row[11]
    t.employee_type = row[12]
    t.employement_status = row[13]
    t.rank = row[14]
    t.original_assignment = row[15]

    t.save
  end

  def self.to_csv(options = {})
  CSV.generate(options) do |csv|
    csv << column_names
    all.each do |employee|
      csv << employee.attributes.values_at(*column_names)
      end
    end
  end


end
