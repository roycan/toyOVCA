class Transaction < ApplicationRecord

  def self.import(file)



    CSV.foreach(file.path, :headers => true, :col_sep => ';') do |row|
      if row.size < 2
          CSV.foreach(file.path, :headers => true, :col_sep => ',') do |row2|
            import2(row2)
          end
          break
      else
        import2(row)
      end

    end
  end

  def self.import2(row)
    t = Transaction.new()
    t.TransactionNo = row[1]
    t.ApplicationDate = row[2]
    t.LeaveType = row[3]
    t.LeaveDetails = row[4]
    t.LeaveStatus = row[5]
    t.DevelopmentType = row[6]
    t.DegreePursued = row[7]
    t.Institution = row[8]
    t.Location = row[9]
    t.Country = row[10]
    t.SponsorDonor = row[11]
    t.LocalAbroad = row[12]
    t.StartDate = row[13]
    t.EndDate = row[14]
    t.Duration = row[15]
    t.ReportForDuty = row[16]
    t.ReturnServiceObligation = row[17]
    t.ReturnServiceObligationStatus = row[18]
    t.save
  end

  def self.to_csv(options = {})
  CSV.generate(options) do |csv|
    csv << column_names
    all.each do |transaction|
      csv << transaction.attributes.values_at(*column_names)
      end
    end
  end

end
