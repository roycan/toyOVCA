class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.integer :employee_no
      t.string :employee_name
      t.date :date_of_birth
      t.string :gender
      t.string :primary_email_address
      t.string :secondary_email_address
      t.integer :primary_contact_no
      t.integer :secondary_contact_no
      t.string :permanent_address
      t.string :unit
      t.string :department
      t.string :employee_type
      t.string :employement_status
      t.string :rank
      t.date :original_assignment

      t.timestamps
    end
  end
end
