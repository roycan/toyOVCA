require 'test_helper'

class TransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @transaction = transactions(:one)
  end

  test "should get index" do
    get transactions_url
    assert_response :success
  end

  test "should get new" do
    get new_transaction_url
    assert_response :success
  end

  test "should create transaction" do
    assert_difference('Transaction.count') do
      post transactions_url, params: { transaction: { ApplicationDate: @transaction.ApplicationDate, Country: @transaction.Country, DegreePursued: @transaction.DegreePursued, DevelopmentType: @transaction.DevelopmentType, Duration: @transaction.Duration, EndDate: @transaction.EndDate, Institution: @transaction.Institution, LeaveStatus: @transaction.LeaveStatus, LeaveType: @transaction.LeaveType, LocalAbroad: @transaction.LocalAbroad, Location: @transaction.Location, ReportForDuty: @transaction.ReportForDuty, ReturnServiceObligation: @transaction.ReturnServiceObligation, ReturnServiceObligationStatus: @transaction.ReturnServiceObligationStatus, SponsorDonor: @transaction.SponsorDonor, StartDate: @transaction.StartDate, TransactionNo: @transaction.TransactionNo } }
    end

    assert_redirected_to transaction_url(Transaction.last)
  end

  test "should show transaction" do
    get transaction_url(@transaction)
    assert_response :success
  end

  test "should get edit" do
    get edit_transaction_url(@transaction)
    assert_response :success
  end

  test "should update transaction" do
    patch transaction_url(@transaction), params: { transaction: { ApplicationDate: @transaction.ApplicationDate, Country: @transaction.Country, DegreePursued: @transaction.DegreePursued, DevelopmentType: @transaction.DevelopmentType, Duration: @transaction.Duration, EndDate: @transaction.EndDate, Institution: @transaction.Institution, LeaveStatus: @transaction.LeaveStatus, LeaveType: @transaction.LeaveType, LocalAbroad: @transaction.LocalAbroad, Location: @transaction.Location, ReportForDuty: @transaction.ReportForDuty, ReturnServiceObligation: @transaction.ReturnServiceObligation, ReturnServiceObligationStatus: @transaction.ReturnServiceObligationStatus, SponsorDonor: @transaction.SponsorDonor, StartDate: @transaction.StartDate, TransactionNo: @transaction.TransactionNo } }
    assert_redirected_to transaction_url(@transaction)
  end

  test "should destroy transaction" do
    assert_difference('Transaction.count', -1) do
      delete transaction_url(@transaction)
    end

    assert_redirected_to transactions_url
  end
end
